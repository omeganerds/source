import urllib
import json
from gtts import gTTS
from PIL import Image
import os

previous_weather_file = "weather_log.txt" #Reads the previous info already stored.
api_key_path = "api_key.txt"	#Read the Key given to the user by the weather website.
previous_weather = ""
language = 'en'

sayThis = 'Hello, Welcome. Are we working on the computer or the web today?'
thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
thingsToSay.save("intro.mp3")
os.system("omxplayer intro.mp3")

print "\nOffline or Online. [off/on]"
offon = raw_input()

if (offon == "on"): 
	#To take the name of the city from user.
	sayThis = 'Enter the name of the city along with the country\'s initials.' 
	thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
	thingsToSay.save("intro_city.mp3")
	os.system("omxplayer intro_city.mp3")
	print "\nEnter the name of city and country that you want the weather of."
	print "\nThe format will be: City_name_with_capital_first_alphabet,country_initials\n"
	city = raw_input()	#Stores the city name entered by the user to the variable city.

	try:	#test to check that we have infoo in the given weather datalog file and api key file.
		log = open(previous_weather_file,"r")
		previous_weather = log.read()
		log.close()
		with open(api_key_path) as f:
			api_key = f.read()	#stores the key id from text file to api_key variable.
	except:
		print "No previous data"	#if the file check test fails.

	if len(api_key) <= 1:	#If the key is not provided to the program.
			#It will ask the user to create account and provide key.
		#print "\napi-key required."
		#print "\nCreate an account at www.openweathermap.org, and add your api-key to './api_key.txt'"
		sayThis = 'Sorry API key was not found. lease create an accountat openweathermap.org and provide me your api key. Good Bye.' 
		thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
		thingsToSay.save("exit.mp3")
		os.system("omxplayer exit.mp3")
		exit(0)

	#The url of the website that provided the weather in a program readable format.
	f = urllib.urlopen("http://api.openweathermap.org/data/2.5/weather?q=%s&APPID=%s" % (city, api_key))
	weather = f.read()	#To read the record given by the Website.

	log = open(previous_weather_file,'w')	#To update the Weather_log.txt
	log.write(weather)
	log.close()

	weather_json = json.loads(weather)	#This program uses the JSON api to read the file.

	curr_temp = float(weather_json['main']['temp'])-273.13
	min_temp = float(weather_json['main']['temp_min'])-273.13
	max_temp = float(weather_json['main']['temp_max'])-273.13

	print "\n\nTemperature in %s is: %.2f degree celcius." % (city[:-3], curr_temp)
	print "\nMinimum Temperature in %s can be %.2f degree celcius." % (city[:-3], min_temp)
	print "\nMaximum Temperature in %s can be %.2f degree celcius.\n" % (city[:-3], max_temp)
	if (not previous_weather == ""):  #Compares the temareture value that we just got with the one that we previously had.
		prev_weather_json = json.loads(previous_weather)
		prev_temp = float(prev_weather_json['main']['temp'])-273.13
		temp_diff = curr_temp - prev_temp
		if (not temp_diff == 0.0):
			print "\nTemperature has changed by: %.2f degree celcius.\n" % (temp_diff)

	sayThis = 'Temperature in %s is: %.0f degree celcius. It can reach the maximum of %.0f degree celcius and a minimum of %.0f degree celcius.' % (city[:-3], curr_temp, max_temp, min_temp)
	thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
	thingsToSay.save("current_max_min_temp.mp3")
	os.system("omxplayer current_max_min_temp.mp3")
	exit(0)


sayThis = 'Do you want to play music or open gallery?'
thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
thingsToSay.save("multimedia.mp3")
os.system("omxplayer multimedia.mp3")

print "\nYour wish is my command. [Music/Gallery]"
multimedia = raw_input()

if ( multimedia == "Music" ):
	sayThis = 'Random music  or any thing in particular?'
	thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
	thingsToSay.save("music.mp3")
	os.system("omxplayer music.mp3")
	print "\nAny thing Particular? [y/n]"
	rp = raw_input()

	if (rp == "n"):
		os.system("omxplayer randommusic.mp3")

	if (rp == "y"):
		sayThis = 'Do you want to play Eminem\'s Rap God or Loose yourself or Week end\'s Star Boy.' 
		thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
		thingsToSay.save("multimedia.mp3")
		os.system("omxplayer multimedia.mp3")

		print "\n Rap God [1], Loose Yourself [2] or Star Boy [3]"
		random = raw_input()

		if (random == "1"):
			os.system("omxplayer rapgod.mp3")
		if (random == "2"):
			os.system("omxplayer looseyourself.mp3")
		if (random == "3"):
                	os.system("omxplayer starboy.mp3")
	exit(0)

if ( multimedia == "Gallery" ):
        sayThis = 'Random picture or any thing in particular?'
        thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
        thingsToSay.save("pic.mp3")
        os.system("omxplayer pic.mp3")
        print "\nAny thing Particular? [y/n]"
        rp = raw_input()

        if (rp == "n"):
		Apple = Image.open("johnny.jpg")
		Apple.show()
	if (rp == "y"):
                sayThis = 'Do you want to see an Island, a Beach or a Road.' 
                thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
                thingsToSay.save("multimedia.mp3")
                os.system("omxplayer multimedia.mp3")

                print "\nIsland [1], Beach [2] or Road [3]"
                random = raw_input()

                if (random == "1"):
                       Apple = Image.open("island.jpg")
	               Apple.show()
                if (random == "2"):
                        Apple = Image.open("beach.jpg")
	                Apple.show()
                if (random == "3"):
                        Apple = Image.open("road.jpg")
	                Apple.show()
        exit(0)
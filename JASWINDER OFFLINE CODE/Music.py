import urllib
import json
from gtts import gTTS
from PIL import Image
import os

previous_weather_file = "weather_log.txt" #Reads the previous info already stored.
api_key_path = "api_key.txt"	#Read the Key given to the user by the weather website.
previous_weather = ""
language = 'en'

sayThis = 'Hello, Welcome. Are we working on the computer or the web today?'
thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
thingsToSay.save("intro.mp3")
os.system("omxplayer intro.mp3")

print "\nOffline or Online. [off/on]"
offon = raw_input()

if (offon == "off"): 

sayThis = 'Do you want to play music or open gallery?'
thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
thingsToSay.save("multimedia.mp3")
os.system("omxplayer multimedia.mp3")

print "\nYour wish is my command. [Music/Gallery]"
multimedia = raw_input()

if ( multimedia == "Music" ):
	sayThis = 'Random music  or any thing in particular?'
	thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
	thingsToSay.save("music.mp3")
	os.system("omxplayer music.mp3")
	print "\nAny thing Particular? [y/n]"
	rp = raw_input()

	if (rp == "n"):
		os.system("omxplayer randommusic.mp3")

	if (rp == "y"):
		sayThis = 'Do you want to play Eminem\'s Rap God or Loose yourself or Week end\'s Star Boy.' 
		thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
		thingsToSay.save("multimedia.mp3")
		os.system("omxplayer multimedia.mp3")

		print "\n Rap God [1], Loose Yourself [2] or Star Boy [3]"
		random = raw_input()

		if (random == "1"):
			os.system("omxplayer rapgod.mp3")
		if (random == "2"):
			os.system("omxplayer looseyourself.mp3")
		if (random == "3"):
                	os.system("omxplayer starboy.mp3")
	exit(0)
# PROJECT – OMEGARON, YOUR PERSONAL A.I. ASSISTANT #

*	Creators: Angad Pal Dhanoa, Jaswinder Singh and Kanwardeep Walia
*	CpE 185 - Lab Section # 2 and # 3
*   Lab Instructor - Eric Telles and Sean Kennedy
*   Lab Day: Wednesday and Tuesday
*	Lab Time: 06:30 P.M.

### Introduction ###

*	This project uses Raspberry Pi to create an interactive system like Google Assistant.
*	The user and the Pi interact with each other and, Pi replies to the user’s commands using internet and/or internal applications.
*	Internet resources are used to search for the weather functionality where the user can ask for the current, minimum and maximum temperature of 500+ cities worldwide.
*	In-built and preinstalled applications are used to play music files (.mp3) and open pictures from the gallery (.jpg). 
*	The command is provided by the user as a speech input and the speech is then recognized and processed accordingly to get converted into a text file.  
*	The Pi uses the text file to process the commands and outputs the result of the command in the form of speech and text.

### Setting Up The The Project ###

*   Some Libraries will need to be installed for the project to work properly.
*   These libraries include gtts, json, pil, urllib, speech_recognition etc.
*   The first step will be to update the raspberry pi itself and then go with the flow.

### Files Included ###

*   For most part all the files that are being used in the project have been uploaded and these also include the files that grew up or were later on integrated in th emain final project.
*   The database can be treated as the stepping stone for all the project parts.


### Who do I talk to? ###


*	**Angad Pal Dhanoa**
*       OmegaRon’s online weather functionality – Weather.	Exchange of information from user to server and vice versa.
*       Extraction of useful data from the information received by the server.   
*       Text-to-speech.
*           Conversion of String statements into speech recognized by the user.
*           Setting the language and speed of the output.

*   **Jaswinder Singh**
       
*             Asking user's input for Offline or Online.
*             Making the external applications work within the command line without any need to leave the main program.
*             Playing music of user's choice.
*             Displaying an image of user's choice.            

*		Screen output.
*			Use of command line screen to output the results of the program results.
 
*   **Kanwardeep Walia**
*		Speech-to-text.
*			User interaction with the program in form of speech input.
*			Voice recognition
*			Formulation of text file input.
*		Integration of shell with python.
*			Making speech to text work with python program.
*        Combination of all the python programs created by Kanwardeep, Angadpal, and Jaswinder.
*            Created one python file and integrated speech input of the user for every command.
*            Created a shell program to run the python program and writing shell commands in a way to remove audio file at the end to free space.
*            Using AngadPal's Python program and Jaswinder's python program and editing them to take voice input text file instead of taking input from the keyboard.
*            Optimizing the python code to work faster by re-using the variables instead of creating new for every command. Moreover, creating and rewriting only 1 audio file for voice input of the user and voice output of the OmegaRon, instead of multiple files.
*            Combining the voice and text output of the Omegaron and making them coordinate with each other.
*            Rewriting how the OmegaRon project welcomes the user and setting up voice and text output commands about how the OmegaRon should reply to the user. Basically, working with how the Raspberry Pi and the user interacts. 
*            Working on the program bugs of the final Omegaron Python file and making sure the program runs smoothly.
*            Using the audio player available in most Linux Operating Systems and modifying the code so that OmegaRon can be run on almost all Linux Operating Systems such as Raspbian, Ubuntu, Mac Os etc.



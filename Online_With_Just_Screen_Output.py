# Creator - Angad Pal Dhanoa
# This is the very first version of the online section of the project.
# The user and the program interact with keyboard inputs from the user and the results of the commands are portrayed on screen.
# For this project two text files need to be created and maintained.
# 1st is the "api_key.txt" which contains the key provided the user to give the user access to the openweathermap.org
#		The key is provided to the user when he/she signs up for the same website and key works as a passcode for the user to get access to the weather data.
# 2nd is the "weather_log.txt", it contains information from the first search. 
#		This file is required for the program to compare the temperature difference between the current temperature of the current search with that of the previous one.
#Python file

import urllib
import json

previous_weather_file = "weather_log.txt" #Reads the previous info already stored.
api_key_path = "api_key.txt"	          #Read the Key given to the user by the weather website. 
previous_weather = ""

#To take the name of the city from user.
print "\nEnter the name of city and country that you want the weather of." 
print "\nThe format will be: City_name_with_capital_first_alphabet,country_initials\n"
city = raw_input()	#Stores the city name entered by the user to the variable city.

try:	#test to check that we have infoo in the given weather datalog file and api key file.
	log = open(previous_weather_file,"r")
	previous_weather = log.read()
	log.close()
	with open(api_key_path) as f:
		api_key = f.read()	#stores the key id from text file to api_key variable.
except:
	print "No previous data"	#if the file check test fails.

if len(api_key) <= 1:	#If the key is not provided to the program. 
			            #It will ask the user to create account and provide key.
	print "\napi-key required."
	print "\nCreate an account at www.openweathermap.org, and add your api-key to './api_key.txt'"
	exit(0)

#The url of the website that provided the weather in a program readable format.
f = urllib.urlopen("http://api.openweathermap.org/data/2.5/weather?q=%s&APPID=%s" % (city, api_key))
weather = f.read()	#To read the record given by the Website.

log = open(previous_weather_file,'w')	#To update the Weather_log.txt
log.write(weather)
log.close()

weather_json = json.loads(weather)	#This program uses the JSON api to read the file.

curr_temp = float(weather_json['main']['temp'])-273.13
min_temp = float(weather_json['main']['temp_min'])-273.13
max_temp = float(weather_json['main']['temp_max'])-273.13

print "\n\nTemperature in %s is: %.2f degree celcius." % (city, curr_temp)
print "\nMinimum Temperature in %s can be %.2f degree celcius." % (city, min_temp)
print "\nMaximum Temperature in %s can be %.2f degree celcius." % (city, max_temp)
if (not previous_weather == ""):  #Compares the temareture value that we just got with the one that we previously had.
	prev_weather_json = json.loads(previous_weather)
	prev_temp = float(prev_weather_json['main']['temp'])-273.13
	temp_diff = curr_temp - prev_temp
	if (not temp_diff == 0.0):
		print "\nTemperature has changed by: %.2f degree celcius.\n" % (temp_diff)

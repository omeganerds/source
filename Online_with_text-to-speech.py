# Creator - Angad Pal Dhanoa
# This Program works in the same manner to that of the other online program but along with all th efeatures from the previous one.
# This can also output the results in the form of speech.
# This program also makes use of the "api_key.txt" and "weather_log.txt" for similar purposes.

import urllib
import json
from gtts import gTTS
import os

previous_weather_file = "weather_log.txt" #Reads the previous info already stored.
api_key_path = "api_key.txt"	#Read the Key given to the user by the weather website.
previous_weather = ""
language = 'en'
#To take the name of the city from user.
print "\nEnter the name of city and country that you want the weather of." 
print "\nThe format will be: City_name_with_capital_first_alphabet,country_initials\n"
city = raw_input()	#Stores the city name entered by the user to the variable city.

try:	#test to check that we have infoo in the given weather datalog file and api key file.
	log = open(previous_weather_file,"r")
	previous_weather = log.read()
	log.close()
	with open(api_key_path) as f:
		api_key = f.read()	#stores the key id from text file to api_key variable.
except:
	print "No previous data"	#if the file check test fails.

if len(api_key) <= 1:	#If the key is not provided to the program. 
			#It will ask the user to create account and provide key.
	print "\napi-key required."
	print "\nCreate an account at www.openweathermap.org, and add your api-key to './api_key.txt'"
	exit(0)

#The url of the website that provided the weather in a program readable format.
f = urllib.urlopen("http://api.openweathermap.org/data/2.5/weather?q=%s&APPID=%s" % (city, api_key))
weather = f.read()	#To read the record given by the Website.

log = open(previous_weather_file,'w')	#To update the Weather_log.txt
log.write(weather)
log.close()

weather_json = json.loads(weather)	#This program uses the JSON api to read the file.

# The temperature is in celsius 
curr_temp = float(weather_json['main']['temp']) - 273.13
min_temp = float(weather_json['main']['temp_min']) - 273.13
max_temp = float(weather_json['main']['temp_max']) - 273.13

print "\n\nTemperature in %s is: %.2f degree celcius." % (city[:-3], curr_temp)
print "\nMinimum Temperature in %s can be %.2f degree celcius." % (city[:-3], min_temp)
print "\nMaximum Temperature in %s can be %.2f degree celcius.\n" % (city[:-3], max_temp)
if (not previous_weather == ""):  # Compares the temareture value that we just got with the one that we previously had.
	prev_weather_json = json.loads(previous_weather)
	prev_temp = float(prev_weather_json['main']['temp'])-273.13
	temp_diff = curr_temp - prev_temp
	if (not temp_diff == 0.0):
		print "\nTemperature has changed by: %.2f degree celcius.\n" % (temp_diff)

sayThis = 'Temperature in %s is: %.0f degree celcius. It can reach the minimum of %.0f degree celcius and a maximum of %.0f degree celcius.' % (city[:-3], curr_temp, min_temp, max_temp)
thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
thingsToSay.save("current_max_min_temp.mp3")
os.system("omxplayer current_max_min_temp.mp3")

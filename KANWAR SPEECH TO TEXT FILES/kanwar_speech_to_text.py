# Creator - Kanwardeep Walia
# In this program, speech file testing.wav created using the shell program (containing user voice) is input.
# The file goes to the Google's API to convert it from the speech to text using Speech_recognition library and returns it as a text string.


import speech_recognition as sr
r = sr.Recognizer()
with sr.WavFile("testing.wav") as source:              # use "test.wav" as the audio source
    audio = r.record(source)                        # extract audio data from the file

# Speech recognition using Google Speech Recognition
try:
    # for testing purposes, we're just using the default API key
    # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
    # instead of `r.recognize_google(audio)`
    print(r.recognize_google(audio))
except sr.UnknownValueError:
    print("Google Speech Recognition could not understand audio")
except sr.RequestError as e:
    print("Could not request results from Google Speech Recognition service; {0}".format(e))


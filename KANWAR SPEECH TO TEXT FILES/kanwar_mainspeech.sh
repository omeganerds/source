# Creator - Kanwardeep Walia
# This program is the main file that needs to run to convert the speech to text. Basically it records the audio using arecord software for 7 seconds.
# Then, it runs the python program kanwar_speech_to_text.py where the speech is converted to text.
# It puts the output of the python program into a text file and then displays it on the screen.
# The end result is that the speech has been converted to text, saved into a text file named as stt.txt and displayed on the screen.

#!/bin/bash

echo "Recording... Press CTRL+C to stop."
arecord -D plughw:1,0 -d 7 -f S16_LE -c2 -r44100 -c 1 testing.wav


echo "Processing ..."
python kanwar_speech_to_text.py > stt.txt


echo -n "You said: "
cat stt.txt

rm testing.wav

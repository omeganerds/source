# Creator - Kanwardeep Walia
# This program is the combination of the python Program of Kanwardeep's speech_to_text.py; Angadpal's Online_with_text-to-speech.py; Jaswinder's offline python file.
# This is the main python program where the speech is taken as input in the speech2text() method and then converted into the text file named as speech2text_converted.txt
# The speech to text converted file is then used to as input for the processing of online and offline commands.
# This program includes various voice and text replies of the OmegaRon to the user. 
# This is the program where the user and Omegaron interact and has modified Angad's and Jaswinder's python files to say step by step things to make it more interactive and automatic process.
# Also, speech input has been added after every processing command.

#Remember, the songs need to be downloaded for the bitbucket code because of copyright purposes. If using different songs, edit the song name in the music_reply section of the code.


import urllib
import json
from gtts import gTTS
import os
import speech_recognition as sr
import sys
from PIL import Image


user_output = 'None'

def speech2text():

#####definition speech2text() method starts #####

	########################### recording, text file creation and saving as output_text starts ##############################

	global user_output
	print "\n\n Speak ..."
	os.system("arecord -q -D plughw:1,0 -d 6 -f S16_LE -c2 -r44100 -c 1 user_input_audio.wav")

	r = sr.Recognizer()
	with sr.WavFile("user_input_audio.wav") as source:              # use "test.wav" as the audio source
   	 audio = r.record(source)                       		 # extract audio data from the file

	
	sound_text_output = sys.stdout
	sys.stdout = open('speech2text_converted.txt' , 'wt')

	try:
		print(r.recognize_google(audio))
	except sr.UnknownValueError:
		print("Sorry, couldn't understand it. Please run the program again!")
	except sr.RequestError as e:
		print("Could not request results from Google Speech Recognition service; {0}".format(e))

	sys.stdout.close()
	sys.stdout = sound_text_output

	user_output = "speech2text_converted.txt"	#Store the user output from speech2text_converted.txt file
	user_output_read = open(user_output,"r")
	user_output = user_output_read.read()
	user_output_read.close()


	if "Sorry, couldn't understand it. Please run the program again!" in user_output:	
		print "\n%s " % (user_output)
		sayThis = '%s    ' % (user_output)
		thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
		thingsToSay.save("user_output_audio.mp3")
		os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")	# Using ffmpeg (ffplay), use omxplayer in pi if this doesn't work.
	
	else:	
		print "\nI recognized that you spoke %s " % (user_output)
		sayThis = 'I recognized that you spoke %s    ' % (user_output)
		thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
		thingsToSay.save("user_output_audio.mp3")
		os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")	# Using ffmpeg (ffplay), use omxplayer in pi if this doesn't work.

	return user_output

	########################### recording, text file creation and saving as output_text  ends ##############################

#####definition speech2text() method ends ###


############## command_processing.py starts ##################

previous_weather_file = "weather_log.txt" #Reads the previous info already stored.
api_key_path = "api_key.txt"	#Read the Key given to the user by the weather website.
previous_weather = ""
language = 'en'

############# added from pitag.py starts-1 ###################

print "Hi, welcome to omegaRon"
sayThis = 'Hi, welcome to omega Ron'
thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
thingsToSay.save("user_output_audio.mp3")
os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")

print "\nWhatever you want me to do, just speak that out!"
sayThis = 'Whatever you want me to do, just speak that out!'
thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
thingsToSay.save("user_output_audio.mp3")
os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")

print "\nSo, would you like to work offline or online?"
sayThis = 'So, would you like to work offline or online?'
thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
thingsToSay.save("user_output_audio.mp3")
os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")


########################### speech2text converted definition method called-0 starts ##############################

speech2text()

########################### speech2text converted definition method called-0 ends #################################

""" ########### Keep This code commented. Tells, what was done starts. ####################

############ omegaRon.sh shell file command added starts recording  ################
os.system("arecord -q -D plughw:1,0 -d 6 -f S16_LE -c2 -r44100 -c 1 user_input_audio.wav")

############ omegaRon.sh shell file command added ends recording################


################# speech2text_processing.py starts #############

r = sr.Recognizer()
with sr.WavFile("user_input_audio.wav") as source:              # use "test.wav" as the audio source
    audio = r.record(source)                        # extract audio data from the file

# Speech recognition using Google Speech Recognition

#####added line#####
sound_text_output = sys.stdout
sys.stdout = open('speech2text_converted.txt' , 'wt')

try:
    # for testing purposes, we're just using the default API key
    # to use another API key, use `r.recognize_google(audio, key="GOOGLE_SPEECH_RECOGNITION_API_KEY")`
    # instead of `r.recognize_google(audio)`
    print(r.recognize_google(audio))
except sr.UnknownValueError:
    print("Sorry, couldn''t understand it. Please give the command again!")
except sr.RequestError as e:
    print("Could not request results from Google Speech Recognition service; {0}".format(e))


####added line#######
sys.stdout.close()
sys.stdout = sound_text_output


 

user_output = "speech2text_converted.txt"	#Store the user output from speech2text_converted.txt file
user_output_read = open(user_output,"r")
user_output = user_output_read.read()
user_output_read.close()

	########### Keep This code commented. Tells, what was done ends. ####################
"""
######################## Testing print statement -1 starts ########################

# print "Out of speech2text def, going to offon if statement"

######################## Testing print statement -1 ends ########################

offon = user_output
if "online" in offon:
	#To take the name of the city from user.
	print "\nCongratulations, you are online and can check the weather of any city!"
	sayThis = 'Congratulations, you are online and can check the weather of any city!'
	thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
	thingsToSay.save("user_output_audio.mp3")
	os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")

	print "\nPlease speak the name of the city for which you want to check the weather?"
	sayThis = 'Please speak the name of the city for which you want to check the weather?' 
	thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
	thingsToSay.save("user_output_audio.mp3")
	os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")

############# added from pitag.py ends-1 ###################


########################### speech2text converted definition method called-1 starts ##############################

	speech2text()

########################### speech2text converted definition method called-1 ends #################################


############## command_processing.py continues ##################


	try:	#test to check that we have infoo in the given weather datalog file and api key file.
		log = open(previous_weather_file,"r")
		previous_weather = log.read()
		log.close()
		with open(api_key_path) as f:
			api_key = f.read()	#stores the key id from text file to api_key variable.
	except:
		print "No previous data"	#if the file check test fails.

	if len(api_key) <= 1:	#If the key is not provided to the program. 
		print "\nSorry API key was not found. Please create an account at openweathermap.org and provide me your api key. Good Bye."
		sayThis = 'Sorry API key was not found. Please create an account at openweathermap.org and provide me your api key. Good Bye.' 
		thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
		thingsToSay.save("user_output_audio.mp3")
		os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")
		exit(0)

	#The url of the website that provided the weather in a program readable format.
	f = urllib.urlopen("http://api.openweathermap.org/data/2.5/weather?q=%s&APPID=%s" % (user_output, api_key))
	weather = f.read()	#To read the record given by the Website.

	log = open(previous_weather_file,'w')	#To update the Weather_log.txt
	log.write(weather)
	log.close()

	weather_json = json.loads(weather)	#This program uses the JSON api to read the file.


	curr_temp = float(weather_json['main']['temp'])-273.13
	min_temp = float(weather_json['main']['temp_min'])-273.13
	max_temp = float(weather_json['main']['temp_max'])-273.13

	print "\n\nTemperature in %s is: %.2f degree celcius." % (user_output, curr_temp) #[:-3]
	print "\nMinimum Temperature in %s can be %.2f degree celcius." % (user_output, min_temp) #[:-3]
	print "\nMaximum Temperature in %s can be %.2f degree celcius.\n" % (user_output, max_temp) #[:-3]

	sayThis = 'Temperature in %s    is: %.0f degree celcius. It can reach the minimum of %.0f degree celcius and a maximum of %.0f degree celcius.' % (user_output, curr_temp, min_temp, max_temp) #[:-3]

	thingsToSay = gTTS(text = sayThis, lang = language, slow = False )

	thingsToSay.save("user_output_audio.mp3")

	os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")
	#os.remove("user_output_audio.mp3")
	exit(0)


if "offline" in offon: 
	print "\nDo you want to play music or open gallery?"
	sayThis = 'Do you want to play music or open gallery?' 
	thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
	thingsToSay.save("user_output_audio.mp3")
	os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")


	########################### speech2text converted definition method called-2 starts ##############################

	speech2text()

	########################### speech2text converted definition method called-2 ends #################################


	multimedia = user_output

	if "play music" in multimedia:
		print "\nRandom music  or any thing in particular?"
		sayThis = 'Random music  or any thing in particular?' 
		thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
		thingsToSay.save("user_output_audio.mp3")
		os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")
	
		"""print "\nIf you want to play random music, please say random music!"
		sayThis = 'If you want to play random music, please say random music!' 
		thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
		thingsToSay.save("user_output_audio.mp3")
		os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")
		"""
		print "\n"	
		print "\nIf you want to play anything in particular, please say yes!"
		sayThis = 'If you want to play anything in particular, please say yes!' 
		thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
		thingsToSay.save("user_output_audio.mp3")
		os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")

########################### speech2text converted definition method called-3 starts ##############################

		speech2text()

########################### speech2text converted definition method called-3 ends #################################

	
		music_reply = user_output

		if "random music" in music_reply:
			os.system("ffplay -nostats -loglevel panic -autoexit -nodisp 'Starboy.mp3' > /dev/null")

		if "yes" in music_reply:
			print "Do you want to play Rap God, Loose yourself or Star Boy."
			sayThis = 'Do you want to play  Rap God,  Loose yourself  or   Star Boy.' 
			thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
			thingsToSay.save("user_output_audio.mp3")
			os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")

			print "\n"
			print "Please speak in the form of first song and so on..."
			sayThis = 'Please speak in the form of first song and so on...' 
			thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
			thingsToSay.save("user_output_audio.mp3")
			os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")

########################### speech2text converted definition method called-4 starts ##############################

			speech2text()

########################### speech2text converted definition method called-4 ends #################################



			song_number = user_output

			if "first song" in song_number:
				os.system("ffplay -nostats -loglevel panic -autoexit -nodisp 'Rap God.mp3' > /dev/null")
			if "second song" in song_number:
				os.system("ffplay -nostats -loglevel panic -autoexit -nodisp 'Loose Yourself.mp3' > /dev/null")
			if "third song" in song_number:
			        os.system("ffplay -nostats -loglevel panic -autoexit -nodisp 'Starboy.mp3' > /dev/null")
		exit(0)



	if "open gallery" in multimedia:
		print "\nRandom picture or any thing in particular?"
		sayThis = 'Random picture or any thing in particular?' 
		thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
		thingsToSay.save("user_output_audio.mp3")
		os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")
        	
		"""print "\nIf you want to open random picture, please say random picture!"
		sayThis = 'If you want to open random picture, please say random picture!' 
		thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
		thingsToSay.save("user_output_audio.mp3")
		os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")
		"""
		print "\n"	
		print "\nIf you want to see anything in particular, please say yes!"
		sayThis = 'If you want to see anything in particular, please say yes!' 
		thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
		thingsToSay.save("user_output_audio.mp3")
		os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")

########################### speech2text converted definition method called-5 starts ##############################

		speech2text()

########################### speech2text converted definition method called-5 ends #################################



		gallery_reply = user_output

		if "random picture" in gallery_reply:
			Apple = Image.open("team.jpg")
			Apple.show()

		if "yes" in gallery_reply:
			print "Do you want to see an Island, a Beach or a Road."
			sayThis = 'Do you want to see an Island, a Beach or a Road.' 
			thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
			thingsToSay.save("user_output_audio.mp3")
			os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")
			
			print "\n"
			print "Please speak in the form of first picture and so on..."
			sayThis = 'Please speak in the form of first picture and so on...' 
			thingsToSay = gTTS(text = sayThis, lang = language, slow = False )
			thingsToSay.save("user_output_audio.mp3")
			os.system("ffplay -nostats -loglevel panic -autoexit -nodisp user_output_audio.mp3 > /dev/null")	
	


########################### speech2text converted definition method called-6 starts ##############################

			speech2text()

########################### speech2text converted definition method called-6 ends #################################



			random_picture = user_output

			if "first picture" in random_picture:
				Apple = Image.open("island.jpg")
				Apple.show()

			if "second picture" in random_picture:
				Apple = Image.open("beach.jpg")
				Apple.show()

			if "third picture" in random_picture:
				Apple = Image.open("road.jpg")
				Apple.show()
		exit(0)


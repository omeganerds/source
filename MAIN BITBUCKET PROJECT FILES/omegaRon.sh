# Creator - Kanwardeep Walia
# This program is the main file that needs to run to open or run Omegaron.
# It runs the python program speeech2text_processing.py where the speech is taken as input and the commands are processed.
# In end, it deletes or removes the user's input audio and Omegaron's output auudio file to free up space in the system.

#!/bin/bash

python speech2text_processing.py


rm user_input_audio.wav
rm user_output_audio.mp3
